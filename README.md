# README #
How to run spring-demo-webmvc application?

Prerequisites: installed and configured **apache maven**

* Run cli command: mvn clean tomcat7:run 

or

* Run cli command: mvn clean install
* in the project folder you will find target directory with spring-demo-webmvc.war file, rename it to chat.war
* install latest apache tomcat, launch it and deploy the chat.war file into it's webapps folder
/**
 *
 */
package eu.lundegaard.chat.dao;

import java.util.List;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
public interface MessageDao {

	/**
	 * @param message
	 * @return
	 */
	Message save(Message message);

	/**
	 * @param sessionId
	 * @return
	 */
	List<Message> findBySessionId(String sessionId);


}

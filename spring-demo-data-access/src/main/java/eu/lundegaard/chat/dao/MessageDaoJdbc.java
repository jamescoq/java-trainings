/**
 *
 */
package eu.lundegaard.chat.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 *
 **/
//	#11 Add annotation that will create spring bean from this class, the bean name should be 'messageDaoJdbc'/
public class MessageDaoJdbc implements MessageDao {

	private JdbcTemplate jdbcTemplate;

	//	#12 create method that will properly configure JdbcTemplate with DataSource reference


	@Override
	public Message save(Message message) {
		String sql = "insert into Message(date, content, author, restricted, receiver, sessionId) values(?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, message.getDate().toString(), message.getContent(), message.getAuthor(), message.getRestricted(),
				message.getReceiver(), message.getSessionId());

		return message;
	}

	//	@Override
	//	public Message save(Message message) {
	//		String sql = "insert into Message(date, content, author, restricted, receiver, sessionId) values(?, ?, ?, ?, ?, ?)";
	//		KeyHolder holder = new GeneratedKeyHolder();
	//
	//		jdbcTemplate.update(new PreparedStatementCreator() {
	//
	//			@Override
	//			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	//				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
	//				ps.setString(1, message.getDate().toString());
	//				ps.setString(2, message.getContent());
	//				ps.setString(3, message.getAuthor());
	//				ps.setBoolean(4, message.getRestricted());
	//				ps.setString(5, message.getReceiver());
	//				ps.setString(6, message.getSessionId());
	//				return ps;
	//			}
	//		}, holder);
	//
	//		Long id = holder.getKey().longValue();
	//		message.setId(id);
	//
	//		return message;
	//	}


	@Override
	public List<Message> findBySessionId(String sessionId) {
		String sql = "select m.* from Message as m where m.sessionId = ?";
		List<Message> messages = jdbcTemplate.query(sql, new String[] { sessionId }, new RowMapper<Message>() {

			@Override
			public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Message(rs.getLong("id"), LocalDate.parse(rs.getString("date")), rs.getString("content"), rs.getString("author"),
						rs.getBoolean("restricted"), rs.getString("receiver"), rs.getString("sessionId"));
			}
		});

		return messages;

	}

}

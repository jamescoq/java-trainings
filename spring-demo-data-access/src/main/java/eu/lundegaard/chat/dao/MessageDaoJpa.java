/**
 *
 */
package eu.lundegaard.chat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
//	#8 Add annotation that will create spring bean from this class, the bean name should be 'messageDaoJpa'
public class MessageDaoJpa implements MessageDao {

	//	#9 add annotation that will inject EntityManagerFactory
	private EntityManagerFactory entityManagerFactory;

	// @PersistenceContext
	// private EntityManager entityManager;

	@Override
	public Message save(Message message) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(message);
		entityManager.getTransaction().commit();
		return message;
	}

	// @Override
	// @Transactional
	// public Message save(Message message) {
	// entityManager.persist(message);
	// return message;
	// }

	@Override
	public List<Message> findBySessionId(String sessionId) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		List<Message> messages = entityManager
				.createQuery("select m from Message m where m.sessionId = ?0", Message.class)
				.setParameter(0, sessionId)
				.getResultList();

		entityManager.getTransaction().commit();
		return messages;
	}

}

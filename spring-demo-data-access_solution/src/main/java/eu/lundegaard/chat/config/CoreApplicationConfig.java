/**
 *
 */
package eu.lundegaard.chat.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author jakubkohout
 *
 */
@Configuration
//	#1 add 'eu.lundegaard.chat.dao' package to component scan
@ComponentScan(basePackages = {"eu.lundegaard.chat.service", "eu.lundegaard.chat.dao"})
@PropertySource(value = "classpath:config/database-settings.properties", ignoreResourceNotFound = true)
//  #2 Import other configuration classes
@Import(value = { DatabaseConfig.class, JpaConfig.class })
public class CoreApplicationConfig {

}

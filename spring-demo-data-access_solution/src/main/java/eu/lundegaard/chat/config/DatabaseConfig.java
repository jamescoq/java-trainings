package eu.lundegaard.chat.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

//	#3 Make the class Spring configuration class
@Configuration
public class DatabaseConfig {

	@Bean
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		//		#7 use addScripts method to initialize database on startup, you can find teh scripts in src/main/resources/sql folder
		return builder
				.addScripts("classpath:sql/create.sql", "classpath:sql/insert.sql")
				.setType(EmbeddedDatabaseType.HSQL)
				.build();
	}

}

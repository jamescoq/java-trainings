/**
 *
 */
package eu.lundegaard.chat.config;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


//	#4 Make the class Spring configuration class
@Configuration
@EnableTransactionManagement
public class JpaConfig {

	@Autowired
	private Environment environment;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		//	#5 scan proper package to find all entities	that shoule be managed by persistence unit
		factory.setPackagesToScan("eu.lundegaard.chat.entity");
		//	#6 add datasource reference
		factory.setDataSource(dataSource);
		factory.setJpaPropertyMap(new HashMap<String, String>() {{
			put("hibernate.dialect", environment.getProperty("db.dialect"));
			put("hibernate.hbm2ddl.auto", environment.getProperty("db.schema.auto_update"));
			put("hibernate.show_sql", environment.getProperty("db.sql.show"));
			put("hibernate.format_sql", environment.getProperty("db.sql.format"));
			put("hibernate.jdbc.batch_size", environment.getProperty("db.jdbc.batch_size"));
			//			 Other possible configutation options
			//		    jpaProperties.put("cache.use_second_level_cache", environment.getProperty("db.jdbc.batch_size"));
			//	        jpaProperties.put("hibernate.cache.use_query_cache", Boolean.FALSE.toString());
			//	        jpaProperties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
			//	        jpaProperties.put("net.sf.ehcache.configurationResourceName", environment.getProperty("core.ehacache.jpa.filepath"));

		}});

		return factory;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		transactionManager.setJpaDialect(new HibernateJpaDialect());

		return transactionManager;
	}
}

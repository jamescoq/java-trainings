package eu.lundegaard.chat.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import eu.lundegaard.chat.dao.MessageDao;
import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;

@Service
public class MessageServiceImpl implements MessageService {

	@Resource(name = "messageDaoJpa")
	private MessageDao messageDao;

	@Override
	public ResponseMessage saveMessage(RequestMessage requestMessage, String authorName, String sessionId) {

		Message message = new Message(requestMessage.getContent(), authorName, requestMessage.getReceiver(), requestMessage.getRestricted(),
				sessionId);

		Message messageEntity = messageDao.save(message);

		return new ResponseMessage(messageEntity.getId(), messageEntity.getDate().toString(), messageEntity.getContent(), authorName);
	}

	@Override
	public List<ResponseMessage> getHistory(String sessionId) {
		List<Message> messages = messageDao.findBySessionId(sessionId);

		return messages.stream()
				.map(m -> new ResponseMessage(m.getId(), m.getDate().toString(), m.getContent(), m.getAuthor()))
				.collect(Collectors.toList());
	}

}

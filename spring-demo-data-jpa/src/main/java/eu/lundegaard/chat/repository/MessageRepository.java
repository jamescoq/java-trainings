package eu.lundegaard.chat.repository;

import java.util.List;

import eu.lundegaard.chat.entity.Message;

// #2 Extend one of the handy Repository interfaces
public interface MessageRepository {

	// #3 Define method that will look up !! by method name !! a List of Message entities by sessionId attribute
	public List<Message> findBySessionId(String sessionId);

	// #6 Define method that will look up !! by Query annotation !! a List of Message entities by sessionId attribute
	public List<Message> findMessagesBySessionId(String sessionId);

	// #7 Define native query method that will look up !! by Query annotation !! a List of Message entities by sessionId
	// attribute
	public List<Message> findMessagesBySessionIdNative(String sessionId);

	// #8 Define method that will look up !! by Query annotation !! a Page of Message entities by sessionId attribute
	// and Pageable attribute

	// #9 Define method that will look up !! by Query annotation !! a List of ResponseMessage dtos by sessionId
	// attribute

}

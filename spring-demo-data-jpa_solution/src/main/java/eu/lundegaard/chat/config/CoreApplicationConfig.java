/**
 *
 */
package eu.lundegaard.chat.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author jakubkohout
 *
 */
@Configuration
@ComponentScan(basePackages = {"eu.lundegaard.chat.service", "eu.lundegaard.chat.dao"})
@PropertySource(value = "classpath:config/database-settings.properties", ignoreResourceNotFound = true)
@Import(value = { DatabaseConfig.class, JpaConfig.class })
public class CoreApplicationConfig {

}

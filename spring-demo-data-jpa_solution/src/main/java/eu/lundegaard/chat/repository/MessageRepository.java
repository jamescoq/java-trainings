package eu.lundegaard.chat.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;

// #2 Extend one of the handy Repository interfaces
public interface MessageRepository extends JpaRepository<Message, Long> {

	// #3 Define method that will look up !! by method name !! a List of Message entities by sessionId attribute
	public List<Message> findBySessionId(String sessionId);

	// #6 Define method that will look up !! by Query annotation !! a List of Message entities by sessionId attribute
	@Query("select m from Message m where m.sessionId = :id")
	public List<Message> findMessagesBySessionId(@Param("id") String sessionId);

	// #7 Define native query method that will look up !! by Query annotation !! a List of Message entities by sessionId
	// attribute
	@Query(value = "select * from MESSAGE where sessionId = ?1", nativeQuery = true)
	public List<Message> findMessagesBySessionIdNative(String sessionId);

	// #8 Define method that will look up !! by Query annotation !! a Page of Message entities by sessionId attribute
	// and Pageable attribute
	@Query("select m from Message m where m.sessionId = :id")
	public Page<Message> findBySessionIdPageable(@Param("id") String sessionId, Pageable pageable);

	// #9 Define method that will look up !! by Query annotation !! a List of ResponseMessage dtos by sessionId
	// attribute
	@Query("select new eu.lundegaard.chat.dto.ResponseMessage(m.id, m.date, m.content, m.author) from Message m where m.sessionId = :id")
	public List<ResponseMessage> findResponseMessageBySessionId(@Param("id") String sessionId);

}

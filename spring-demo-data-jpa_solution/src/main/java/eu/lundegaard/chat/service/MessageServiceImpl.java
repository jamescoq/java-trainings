package eu.lundegaard.chat.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.lundegaard.chat.dao.MessageDao;
import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;
import eu.lundegaard.chat.repository.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService {

	@Resource(name = "messageDaoJpa")
	private MessageDao messageDao;

	@Autowired
	private MessageRepository messageRepository;

	@Override
	public ResponseMessage saveMessage(RequestMessage requestMessage, String authorName, String sessionId) {

		Message message = new Message(requestMessage.getContent(), authorName, requestMessage.getReceiver(), requestMessage.getRestricted(),
				sessionId);

		// #4 replace messageDao for messageRepository
		Message messageEntity = messageRepository.save(message);
		// Message messageEntity = messageDao.save(message);

		return new ResponseMessage(messageEntity.getId(), messageEntity.getDate(), messageEntity.getContent(), authorName);
	}

	@Override
	public List<ResponseMessage> getHistory(String sessionId) {

		// #5 replace messageDao for messageRepository
		List<Message> messages = messageRepository.findBySessionId(sessionId);
		// List<Message> messages = messageDao.findBySessionId(sessionId);

		return messages.stream().map(m -> new ResponseMessage(m.getId(), m.getDate(), m.getContent(), m.getAuthor()))
				.collect(Collectors.toList());
	}

}

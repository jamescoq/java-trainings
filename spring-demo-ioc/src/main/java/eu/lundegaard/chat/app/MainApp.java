/**
 *
 */
package eu.lundegaard.chat.app;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.service.MessageService;

/**
 * @author jakubkohout
 *
 * Main class that you can run as Java Application
 */
public class MainApp {

	private static Logger logger = Logger.getLogger(MainApp.class);

	public static void main(String[] args) {

		// (1) Fill in the correct implementation of ApplicationContext that is able to load Java config bean
		// definitions
		ApplicationContext applicationContext = null;
		// (*) Create application context from Xml file
		MessageService messageService = applicationContext.getBean(MessageService.class);
		RequestMessage requestMessage = new RequestMessage("hello world");

		// (*) copy logger method multiple times after you set prototype scope in CoreApplicationConfig, to see how many
		// times the Dao gets instantiated
		logger.info(messageService.saveMessage(requestMessage, "Jakub Kohout", "gd7ad2dh29l"));
	}
}

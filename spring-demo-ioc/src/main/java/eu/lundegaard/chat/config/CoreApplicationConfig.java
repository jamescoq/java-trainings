/**
 *
 */
package eu.lundegaard.chat.config;

import eu.lundegaard.chat.dao.MessageDao;
import eu.lundegaard.chat.dao.MessageDaoJpa;

/**
 * @author jakubkohout
 *
 */
// (2) Make the class Spring configuration class
// (3) Add component scan targeted on 'eu.lundegaard.chat.service' package
public class CoreApplicationConfig {

	// (4) Create MessageDaoJpa bean definition
	public MessageDao messageDaoJpa() {
		return new MessageDaoJpa();
	}

	// (*) Create MessageDaoHibernate bean definition
	// (*) Specify prototype scope, no-proxy and proxy mode on MessageDaoHibernate
	// (*) User Primary annotation to specify bean injection

}

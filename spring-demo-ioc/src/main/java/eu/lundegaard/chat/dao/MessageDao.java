/**
 *
 */
package eu.lundegaard.chat.dao;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
public interface MessageDao {

	/**
	 * @param message
	 * @return
	 */
	Message save(Message message);


}

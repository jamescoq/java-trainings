/**
 *
 */
package eu.lundegaard.chat.dao;

import org.apache.log4j.Logger;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
public class MessageDaoHibernate implements MessageDao {

	private static Logger logger = Logger.getLogger(MessageDaoHibernate.class);

	public MessageDaoHibernate() {
		logger.info("MessageDaoHibernate constuctor called.");
	}

	@Override
	public Message save(Message message) {
		message.setId(2l);
		return message;
	}
}

/**
 *
 */
package eu.lundegaard.chat.dao;

import org.apache.log4j.Logger;

import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
public class MessageDaoJpa implements MessageDao {

	private static Logger logger = Logger.getLogger(MessageDaoJpa.class);

	public MessageDaoJpa() {
		logger.info("MessageDaoJpa constuctor called.");
	}

	@Override
	public Message save(Message message) {
		message.setId(1l);
		return message;
	}

}

package eu.lundegaard.chat.entity;

import java.io.Serializable;
import java.time.LocalDate;

public class Message implements Serializable {

	private Long id;
	private LocalDate date;
	private String content;
	private String author;
	private Boolean restricted;
	private String receiver;
	private String sessionId;

	public Message() {
	}

	public Message(String content, String author, String receiver, Boolean restricted, String sessionId) {
		this.content = content;
		this.author = author;
		this.receiver = receiver;
		this.restricted = restricted;
		this.date = LocalDate.now();
		this.sessionId = sessionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Boolean getRestricted() {
		return restricted;
	}

	public void setRestricted(Boolean restricted) {
		this.restricted = restricted;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", date=" + date + ", content=" + content + ", author=" + author + ", restricted=" + restricted
				+ ", receiver=" + receiver + ", sessionId=" + sessionId + "]";
	}

}

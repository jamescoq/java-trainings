package eu.lundegaard.chat.service;

import eu.lundegaard.chat.dao.MessageDao;
import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;


// (5)	Anotate the bean as a Service bean
public class MessageServiceImpl implements MessageService{

	//	(6) Add annotation that will manage automatic injection of MessageDao bean
	//	(*) User Qualifier and Resource annotations to specify bean injection
	private MessageDao messageDaoJpa;

	@Override
	public ResponseMessage saveMessage(RequestMessage requestMessage, String authorName, String sessionId) {

		Message message = new Message(requestMessage.getContent(), authorName, requestMessage.getReceiver(), requestMessage.getRestricted(),
				sessionId);

		Message persistedMessage = messageDaoJpa.save(message);

		return new ResponseMessage(persistedMessage.getId(), persistedMessage.getDate().toString(), persistedMessage.getContent(),
				authorName);
	}

}

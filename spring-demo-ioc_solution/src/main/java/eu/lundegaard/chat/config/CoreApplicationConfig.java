/**
 *
 */
package eu.lundegaard.chat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import eu.lundegaard.chat.dao.MessageDao;
import eu.lundegaard.chat.dao.MessageDaoHibernate;
import eu.lundegaard.chat.dao.MessageDaoJpa;

/**
 * @author jakubkohout
 *
 */
// (2) Make the class Spring configuration class
@Configuration
// (3) Add component scan targeted on 'eu.lundegaard.chat.service' package
@ComponentScan(basePackages = "eu.lundegaard.chat.service")
public class CoreApplicationConfig {

	// (4) Create MessageDaoJpa bean definition
	@Bean
	public MessageDao messageDaoJpa() {
		return new MessageDaoJpa();
	}

	// (*) Create MessageDaoHibernate bean definition
	// (*) Specify prototype scope, no-proxy and proxy mode on MessageDaoHibernate
	// (*) User Primary annotation to specify bean injection
	@Bean
	@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
	public MessageDao messageDaoHibernate() {
		return new MessageDaoHibernate();
	}

}

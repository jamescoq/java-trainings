package eu.lundegaard.chat.service;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;

public interface MessageService {

	ResponseMessage saveMessage(RequestMessage message, String name,  String sessionId);

}

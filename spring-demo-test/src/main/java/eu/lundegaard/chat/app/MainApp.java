/**
 *
 */
package eu.lundegaard.chat.app;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import eu.lundegaard.chat.config.CoreApplicationConfig;
import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.service.MessageService;

/**
 * @author jakubkohout
 *
 * Main class that you can run as Java Application
 */
public class MainApp {

	private static Logger logger = Logger.getLogger(MainApp.class);

	public static void main(String[] args) {

		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(CoreApplicationConfig.class);

		MessageService messageService = applicationContext.getBean(MessageService.class);
		RequestMessage requestMessage = new RequestMessage("hello world");

		logger.info(messageService.saveMessage(requestMessage, "Jakub Kohout", "gd7ad2dh29l"));

		messageService.getHistory("0123qwer").stream().forEach(m -> logger.info(m));
	}
}

/**
 * 
 */
package eu.lundegaard.chat.service;

import java.util.List;

import org.junit.Assert;

import eu.lundegaard.chat.dto.ResponseMessage;

/**
 * @author jakubkohout
 *
 */
// #3 extend your class where you bootstrap spring application context
public class MessageServiceTest {

	private static final String NULL_SESSIONID = null;
	private static final String CORRECT_SESSIONID = "0123qwer";
	private static final String INCORRECT_SESSIONID = "qwer0123";
	private static final String WRONG_LENGTH_SESSIONID = "qwer";

	// #4 Inject MessageService.class from bootstrapped application context
	private MessageService messageService;

	// #5 Mark method as a test method with proper annotation, this test should expect some exception to be thrown
	public void testGetHistoryWithNullArgs() {
		messageService.getHistory(NULL_SESSIONID);
	}

	// #6 Mark method as a test method with proper annotation
	public void testGetHistoryWithCorrectArgs() {
		List<ResponseMessage> messages = messageService.getHistory(CORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(5, messages.size());
	}

	// #7 Mark method as a test method with proper annotation
	public void testGetHistoryWithIncorrectArgs() {
		List<ResponseMessage> messages = messageService.getHistory(INCORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(0, messages.size());
	}

	// #8 Mark method as a test method with proper annotation, this test should expect some exception to be thrown
	public void testGetHistoryWithWrongLengthArgs() {
		messageService.getHistory(WRONG_LENGTH_SESSIONID);
	}

}

package eu.lundegaard.chat.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

//#10 Annotate the class so that it can become Entity class managed by Pesistence Unit
@Entity
public class Message implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGE_SEQUENCE")
	@SequenceGenerator(name = "MESSAGE_SEQUENCE", sequenceName = "SEQUENCE_MESSAGE", allocationSize = 1)
	private Long id;
	private LocalDate date;
	private String content;
	private String author;
	private Boolean restricted;
	private String receiver;
	private String sessionId;

	public Message() {
	}

	public Message(Long id, LocalDate date, String content, String author, Boolean restricted, String receiver, String sessionId) {
		this.id = id;
		this.date = date;
		this.content = content;
		this.author = author;
		this.restricted = restricted;
		this.receiver = receiver;
		this.sessionId = sessionId;
	}

	public Message(String content, String author, String receiver, Boolean restricted, String sessionId) {
		this.content = content;
		this.author = author;
		this.receiver = receiver;
		this.restricted = restricted;
		this.date = LocalDate.now();
		this.sessionId = sessionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Boolean getRestricted() {
		return restricted;
	}

	public void setRestricted(Boolean restricted) {
		this.restricted = restricted;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", date=" + date + ", content=" + content + ", author=" + author + ", restricted=" + restricted
				+ ", receiver=" + receiver + ", sessionId=" + sessionId + "]";
	}

}

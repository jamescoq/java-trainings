package eu.lundegaard.chat.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;
import eu.lundegaard.chat.repository.MessageRepository;

@Service
@Transactional(readOnly = true)
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageRepository messageRepository;

	@Override
	public ResponseMessage saveMessage(RequestMessage requestMessage, String authorName, String sessionId) {

		Message message = new Message(requestMessage.getContent(), authorName, requestMessage.getReceiver(), requestMessage.getRestricted(),
				sessionId);

		Message messageEntity = messageRepository.save(message);

		return new ResponseMessage(messageEntity.getId(), messageEntity.getDate(), messageEntity.getContent(), authorName);
	}

	@Override
	public List<ResponseMessage> getHistory(String sessionId) {
		Assert.hasLength(sessionId, "SessionId cannot be null or empty");
		Assert.isTrue(sessionId.length() == 8, "SessionId must have length of 8");

		List<Message> messages = messageRepository.findBySessionId(sessionId);

		return messages.stream().map(m -> new ResponseMessage(m.getId(), m.getDate(), m.getContent(), m.getAuthor()))
				.collect(Collectors.toList());
	}

}

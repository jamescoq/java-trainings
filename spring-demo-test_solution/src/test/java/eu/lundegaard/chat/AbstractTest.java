/**
 * 
 */
package eu.lundegaard.chat;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.lundegaard.chat.config.CoreApplicationConfig;

/**
 * @author jakubkohout
 *
 */
// #1 Enable Spring application context bootstrapping support with SpringJUnit4ClassRunner.class
// #2 Add annotation that loads spring configuration classes needed for test application context bootstrap
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CoreApplicationConfig.class })
@Ignore
public class AbstractTest {

}

/**
 * 
 */
package eu.lundegaard.chat.service;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.lundegaard.chat.dto.ResponseMessage;

/**
 * @author jakubkohout
 *
 */
public class MessageServiceMockTest {

	private static final String NULL_SESSIONID = null;
	private static final String CORRECT_SESSIONID = "0123qwer";
	private static final String INCORRECT_SESSIONID = "qwer0123";
	private static final String WRONG_LENGTH_SESSIONID = "qwer";

	private MessageService messageService;

	@Before
	public void init() {
		// #9 Create mock of MessageService.class
		this.messageService = EasyMock.createNiceMock(MessageService.class);
	}

	@After
	public void destroy() {
		EasyMock.resetToNice(messageService);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithNullArgs() {
		// #10 make expectations about messageService.getHistory() method call and its return type
		EasyMock.expect(messageService.getHistory(NULL_SESSIONID)).andThrow(new IllegalArgumentException());
		// #11 replay mock behavior
		EasyMock.replay(messageService);

		messageService.getHistory(NULL_SESSIONID);

		// #12 verify mock performance
		EasyMock.verify(messageService);
	}

	// #13 add test to test testGetHistoryWithCorrectArgs like in MessageServiceTest.class but by using mocks
	@Test
	public void testGetHistoryWithCorrectArgs() {

		EasyMock.expect(messageService.getHistory(CORRECT_SESSIONID)).andReturn(TestData.getTestMessages());

		EasyMock.replay(messageService);

		List<ResponseMessage> messages = messageService.getHistory(CORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(5, messages.size());

		EasyMock.verify(messageService);
	}

	// #14 add test to test testGetHistoryWithIncorrectArgs like in MessageServiceTest.class but by using mocks
	@Test
	public void testGetHistoryWithIncorrectArgs() {
		EasyMock.expect(messageService.getHistory(INCORRECT_SESSIONID)).andReturn(new ArrayList<>());

		EasyMock.replay(messageService);

		List<ResponseMessage> messages = messageService.getHistory(INCORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(0, messages.size());

		EasyMock.verify(messageService);
	}

	// #15 add test to test testGetHistoryWithWrongLengthArgs like in MessageServiceTest.class but by using mocks
	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithWrongLengthArgs() {
		EasyMock.expect(messageService.getHistory(WRONG_LENGTH_SESSIONID)).andThrow(new IllegalArgumentException());

		EasyMock.replay(messageService);

		messageService.getHistory(WRONG_LENGTH_SESSIONID);

		EasyMock.verify(messageService);
	}

	static class TestData {

		public static List<ResponseMessage> getTestMessages() {
			return new ArrayList() {
				{
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
				}
			};
		}
	}
}

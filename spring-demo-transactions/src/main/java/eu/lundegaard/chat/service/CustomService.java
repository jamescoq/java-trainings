/**
 * 
 */
package eu.lundegaard.chat.service;


/**
 * @author jakubkohout
 *
 */
public interface CustomService {

	void proceedInNewTransaction();

}

/**
 * 
 */
package eu.lundegaard.chat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.lundegaard.chat.repository.MessageRepository;

/**
 * @author jakubkohout
 *
 */
@Service
public class CustomServiceImpl implements CustomService {

	@Autowired
	private MessageRepository messageRepository;

	@Override
	// declare @Transactional with propagation REQUIRES_NEW
	public void proceedInNewTransaction() {
		messageRepository.findAll();
	}

}

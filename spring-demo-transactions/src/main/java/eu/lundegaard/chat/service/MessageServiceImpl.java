package eu.lundegaard.chat.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;
import eu.lundegaard.chat.repository.MessageRepository;

@Service
// #1 launch eu.lundegaard.chat.service.MessageServiceTest.testUpdateMessage() and inspect the log, look where the
// transaction is created
// #2 declare read only transaction on class level and launch
// eu.lundegaard.chat.service.MessageServiceTest.testUpdateMessage(), inspect the log, look where the transaction is
// created
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private CustomService customService;

	@Override
	public ResponseMessage saveMessage(RequestMessage requestMessage, String authorName, String sessionId) {

		Message message = new Message(requestMessage.getContent(), authorName, requestMessage.getReceiver(), requestMessage.getRestricted(),
				sessionId);

		Message messageEntity = messageRepository.save(message);

		return new ResponseMessage(messageEntity.getId(), messageEntity.getDate(), messageEntity.getContent(), authorName);
	}

	@Override
	public List<ResponseMessage> getHistory(String sessionId) {

		List<Message> messages = messageRepository.findBySessionId(sessionId);

		return messages.stream().map(m -> new ResponseMessage(m.getId(), m.getDate(), m.getContent(), m.getAuthor()))
				.collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public Message findMessage(Long id) {
		return messageRepository.findOne(id);
	}

	// #3 declare @Transactional and launch eu.lundegaard.chat.service.MessageServiceTest.testUpdateMessage() and see
	// the log
	@Override
	public void updateMessage(Long id, String text) {
		Message message = messageRepository.findOne(id);
		message.setContent(text);
	}

	// #4 declare @Transactional and launch
	// eu.lundegaard.chat.service.MessageServiceTest.testUpdateMessage2() and see the log
	@Override
	public void updateMessage(Message message, String text) {
		message.setContent(text);
	}

	@Override
	// #5 launch the eu.lundegaard.chat.service.MessageServiceTest.testUpdateMessag3() test and see the log
	// #6 go to eu.lundegaard.chat.service.CustomServiceImpl.proceedInNewTransaction() and declare @Transactional with
	// propagation REQUIRES_NEW and launch the test again
	public void updateMessage2(Long id, String text) {
		Message message = findMessage(id);
		customService.proceedInNewTransaction();
		message.setContent(text);
	}

}

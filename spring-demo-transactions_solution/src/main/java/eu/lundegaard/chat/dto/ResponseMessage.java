package eu.lundegaard.chat.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class ResponseMessage implements Serializable {

	private Long id;
	private LocalDate date;
	private String content;
	private String user;

	public ResponseMessage() {
	}

	public ResponseMessage(Long id, LocalDate date, String content, String user) {
		this.id = id;
		this.date = date;
		this.content = content;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ResponseMessage [id=" + id + ", date=" + date + ", content=" + content + ", user=" + user + "]";
	}

}

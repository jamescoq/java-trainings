package eu.lundegaard.chat.service;

import java.util.List;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;

public interface MessageService {

	ResponseMessage saveMessage(RequestMessage message, String name,  String sessionId);

	List<ResponseMessage> getHistory(String name);

	Message findMessage(Long id);

	void updateMessage(Long id, String text);

	void updateMessage(Message message, String text);

	void updateMessage2(Long id, String text);

}

/**
 * 
 */
package eu.lundegaard.chat;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.lundegaard.chat.config.CoreApplicationConfig;

/**
 * @author jakubkohout
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CoreApplicationConfig.class })
public class AbstractTest {

}

/**
 * 
 */
package eu.lundegaard.chat.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.lundegaard.chat.AbstractTest;
import eu.lundegaard.chat.entity.Message;

/**
 * @author jakubkohout
 *
 */
public class MessageServiceTest extends AbstractTest {

	@Autowired
	private MessageService messageService;

	// one transaction automatic update of entity and propagation into DB
	// second launch
	@Test
	public void testUpdateMessage() {
		messageService.updateMessage(1L, "new message");
	}

	// one transaction, but no automatic propagation of changes, because the entity gets detached when exiting
	// findMessage method
	@Test
	public void testUpdateMessage2() {
		Message message = messageService.findMessage(1l);
		messageService.updateMessage(message, "new message");
	}

	// 2 transactions, one gets suspended until another is comitted
	@Test
	public void testUpdateMessag3() {
		messageService.updateMessage2(1L, "new message");
	}

}

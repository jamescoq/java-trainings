package eu.lundegaard.chat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


//	#3 Mark the class as a spring configuration bean
//	#4 enable web support by annotation
@ComponentScan(basePackages = "eu.lundegaard.chat.web")
//	#5 extend proper adapter class to enable configuration of configureDefaultServletHandling and uncomment the method
public class WebConfig {

	//	#6 configure view resolver bean (InternalResourceViewResolver)
	@Bean
    public ViewResolver getViewResolver(){
      return null;
    }
	
//	@Override
//    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }   
	
}

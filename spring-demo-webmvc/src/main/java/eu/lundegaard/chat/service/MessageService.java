package eu.lundegaard.chat.service;

import java.util.List;

import eu.lundegaard.chat.dto.RequestMessage;
import eu.lundegaard.chat.dto.ResponseMessage;

public interface MessageService {

	ResponseMessage saveMessage(RequestMessage message, String name,  String sessionId);

	List<ResponseMessage> getHistory(String name);

	List<ResponseMessage> getAll();

}

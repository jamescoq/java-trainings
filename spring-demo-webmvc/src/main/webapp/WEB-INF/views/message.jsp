<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
<title>Spring web MVC demo app</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<h1>Spring web MVC demo app</h1>
	<p>Welcome to JSP world </p>
	
	<ul>
		<c:forEach items="${messages}" var="message">
			<li>${message.user}: <fmt:formatDate value="${message.utilDate}"/> - ${message.content} </li>
		</c:forEach>
	</ul>
</body>
</html>

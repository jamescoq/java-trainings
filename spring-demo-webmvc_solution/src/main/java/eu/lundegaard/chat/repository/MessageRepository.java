package eu.lundegaard.chat.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

	public List<Message> findBySessionId(String sessionId);

	@Query("select m from Message m where m.sessionId = :id")
	public List<Message> findMessagesBySessionId(@Param("id") String sessionId);

	@Query(value = "select * from MESSAGE where sessionId = ?1", nativeQuery = true)
	public List<Message> findMessagesBySessionIdNative(String sessionId);

	@Query("select m from Message m where m.sessionId = :id")
	public Page<Message> findBySessionIdPageable(@Param("id") String sessionId, Pageable pageable);

	@Query("select new eu.lundegaard.chat.dto.ResponseMessage(m.id, m.date, m.content, m.author) from Message m where m.sessionId = :id")
	public List<ResponseMessage> findResponseMessageBySessionId(@Param("id") String sessionId);

}

package eu.lundegaard.chat.web;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.lundegaard.chat.service.MessageService;

// # Mark class as a controller bean 
@Controller
// # add request mapping to intercept requests with path '/message'
@RequestMapping(value = "/message")
public class MessageController {

	@Autowired
	private MessageService messageService;

	//	0123qwer
	@RequestMapping(method = RequestMethod.GET)
	// # bind possible sessionId request param from request url into sessionId attribute		
	public String list(@RequestParam(name= "sessionId", required = false) String sessionId, Model model) {

		model.addAttribute("messages", (StringUtils.isNotBlank(sessionId)) ? messageService.getHistory(sessionId) : messageService.getAll());
		
		return "message";
	}
	
}

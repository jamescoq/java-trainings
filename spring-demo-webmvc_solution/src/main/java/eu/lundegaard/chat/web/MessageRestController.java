/**
 * 
 */
package eu.lundegaard.chat.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.service.MessageService;

/**
 * @author jakubkohout
 *
 */
//# Mark class as a rest controller bean 
@RestController
//# add request mapping to intercept requests with path '/api'
@RequestMapping(value = "/api")
public class MessageRestController {

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/message", method = RequestMethod.GET)
	public List<ResponseMessage> list() {
		return messageService.getAll();
	}
	
	//	0123qwer
	@RequestMapping(value = "/message/{sessionId}", method = RequestMethod.GET)
	// # bind sessionId property from path into sessionId attribute		
	public List<ResponseMessage> listBySessionId(@PathVariable String sessionId) {
		return messageService.getHistory(sessionId);
	}
}

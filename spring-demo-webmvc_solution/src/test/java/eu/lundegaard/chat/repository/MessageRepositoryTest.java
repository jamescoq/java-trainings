/**
 * 
 */
package eu.lundegaard.chat.repository;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notEmpty;
import static org.springframework.util.Assert.notNull;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import eu.lundegaard.chat.AbstractTest;
import eu.lundegaard.chat.dto.ResponseMessage;
import eu.lundegaard.chat.entity.Message;

public class MessageRepositoryTest extends AbstractTest {

	private static final String SESSION_ID = "0123qwer";
	private static final String SESSION_ID_2 = "4567zxcv";

	@Autowired
	private MessageRepository messageRepository;

	@Test
	public void testFindBySessionId() {
		List<Message> messages = messageRepository.findBySessionId(SESSION_ID_2);
		List<Message> messagesJpq = messageRepository.findMessagesBySessionId(SESSION_ID_2);
		List<Message> messagesSql = messageRepository.findMessagesBySessionIdNative(SESSION_ID_2);

		notEmpty(messages);
		notEmpty(messagesJpq);
		notEmpty(messagesSql);

		isTrue(messages.size() == messagesJpq.size() && messagesJpq.size() == messagesSql.size());
	}

	@Test
	public void testFindFirstFiveMessages() {
		Page<Message> page = messageRepository.findAll(new PageRequest(0, 5));

		notNull(page);
		isTrue(page.getNumber() == 0);
		isTrue(page.getTotalElements() == 14);
		isTrue(page.getNumberOfElements() == 5);
	}

	@Test
	public void testFindSecondFiveMessagesBySessionID() {
		Page<Message> page = messageRepository.findBySessionIdPageable(SESSION_ID_2, (new PageRequest(1, 5)));

		notNull(page);
		isTrue(page.getNumber() == 1);
		isTrue(page.getTotalElements() == 9);
		isTrue(page.getNumberOfElements() == 4);
	}

	@Test
	public void testFindSortableMessages() {
		List<Message> messages = messageRepository.findAll(new Sort(Direction.DESC, "date"));

		notEmpty(messages);
		isTrue(messages.get(0).getDate().equals(LocalDate.of(2016, 8, 27)));
	}

	@Test
	public void testFindResponseMessages() {
		List<ResponseMessage> responseMessages = messageRepository.findResponseMessageBySessionId(SESSION_ID);

		notEmpty(responseMessages);
		isTrue(responseMessages.size() == 5);
	}

}

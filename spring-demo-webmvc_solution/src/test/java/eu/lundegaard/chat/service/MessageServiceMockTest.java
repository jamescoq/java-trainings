/**
 * 
 */
package eu.lundegaard.chat.service;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.lundegaard.chat.dto.ResponseMessage;

/**
 * @author jakubkohout
 *
 */
public class MessageServiceMockTest {

	private static final String NULL_SESSIONID = null;
	private static final String CORRECT_SESSIONID = "0123qwer";
	private static final String INCORRECT_SESSIONID = "qwer0123";
	private static final String WRONG_LENGTH_SESSIONID = "qwer";

	private MessageService messageService;

	@Before
	public void init() {
		this.messageService = EasyMock.createNiceMock(MessageService.class);
	}

	@After
	public void destroy() {
		EasyMock.resetToNice(messageService);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithNullArgs() {
		EasyMock.expect(messageService.getHistory(NULL_SESSIONID)).andThrow(new IllegalArgumentException());
		EasyMock.replay(messageService);

		messageService.getHistory(NULL_SESSIONID);

		EasyMock.verify(messageService);
	}

	@Test
	public void testGetHistoryWithCorrectArgs() {

		EasyMock.expect(messageService.getHistory(CORRECT_SESSIONID)).andReturn(TestData.getTestMessages());

		EasyMock.replay(messageService);

		List<ResponseMessage> messages = messageService.getHistory(CORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(5, messages.size());

		EasyMock.verify(messageService);
	}

	@Test
	public void testGetHistoryWithIncorrectArgs() {
		EasyMock.expect(messageService.getHistory(INCORRECT_SESSIONID)).andReturn(new ArrayList<>());

		EasyMock.replay(messageService);

		List<ResponseMessage> messages = messageService.getHistory(INCORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(0, messages.size());

		EasyMock.verify(messageService);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithWrongLengthArgs() {
		EasyMock.expect(messageService.getHistory(WRONG_LENGTH_SESSIONID)).andThrow(new IllegalArgumentException());

		EasyMock.replay(messageService);

		messageService.getHistory(WRONG_LENGTH_SESSIONID);

		EasyMock.verify(messageService);
	}

	static class TestData {

		public static List<ResponseMessage> getTestMessages() {
			return new ArrayList() {
				{
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
					add(new ResponseMessage());
				}
			};
		}
	}
}

/**
 * 
 */
package eu.lundegaard.chat.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.lundegaard.chat.AbstractTest;
import eu.lundegaard.chat.dto.ResponseMessage;

/**
 * @author jakubkohout
 *
 */
public class MessageServiceTest extends AbstractTest {

	private static final String NULL_SESSIONID = null;
	private static final String CORRECT_SESSIONID = "0123qwer";
	private static final String INCORRECT_SESSIONID = "qwer0123";
	private static final String WRONG_LENGTH_SESSIONID = "qwer";

	@Autowired
	private MessageService messageService;

	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithNullArgs() {
		messageService.getHistory(NULL_SESSIONID);
	}

	@Test
	public void testGetHistoryWithCorrectArgs() {
		List<ResponseMessage> messages = messageService.getHistory(CORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(5, messages.size());
	}

	@Test
	public void testGetHistoryWithIncorrectArgs() {
		List<ResponseMessage> messages = messageService.getHistory(INCORRECT_SESSIONID);

		Assert.assertNotNull(messages);
		Assert.assertEquals(0, messages.size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetHistoryWithWrongLengthArgs() {
		messageService.getHistory(WRONG_LENGTH_SESSIONID);
	}

}
